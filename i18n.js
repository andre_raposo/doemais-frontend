module.exports = {
  locales: ['en', 'pt'],
  defaultLocale: 'en',
  pages: {
    '*': ['common'],
    '/signup': ['signup'],
  },
  loadLocaleFrom: (lang, ns) =>
    import(`${__dirname}/locales/${lang}/${ns}.json`).then((m) => m.default),
};
