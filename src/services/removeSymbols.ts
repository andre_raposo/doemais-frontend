export const removeSymbols = (s: string) =>
  s.replace(/[-&\/\\#,+()$~%.'":*?<>{}\s]/g, '');
