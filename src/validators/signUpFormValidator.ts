import * as Yup from 'yup';
import * as cpf from 'cpf';
import useTranslation from 'next-translate/useTranslation';
import {nameValidation, phoneValidation} from '../constants';

function signUpValidator() {
  const {t} = useTranslation();

  return Yup.object().shape({
    username: Yup.string()
        .required(t('common:requiredError'))
        .min(3, t('common:usernameLengthError')),
    email: Yup.string()
        .required(t('common:requiredError'))
        .email(t('common:emailInvalidError')),
    password: Yup.string()
        .required(t('common:requiredError'))
        .min(8, t('common:passwordLengthError')),
    name: Yup.string()
        .required(t('common:requiredError'))
        .matches(nameValidation, t('common:nameInvalidError')),
    gender: Yup.string().required(t('common:requiredError')),
    cpf: Yup.string()
        .required(t('common:requiredError'))
        .test('cpfValidation', t('signup:invalidCpf'), (value: string) =>
          cpf.isValid(value),
        ),
    birthDate: Yup.date()
        .required(t('common:requiredError'))
        .max(new Date(), t('common:birthDateError')),
    cellphone: Yup.string()
        .required(t('common:requiredError'))
        .matches(phoneValidation, t('common:cellphoneInvalidError')),
  });
}

export default signUpValidator;
