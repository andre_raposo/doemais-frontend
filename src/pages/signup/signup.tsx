import Head from 'next/head';
import SignUpForm from './signUpForm';
import styles from '../../styles/pages/SignUp.module.css';

function signup() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Doe+ | Cadastro</title>
      </Head>
      <SignUpForm />
    </div>
  );
}

export default signup;
