import {Formik, Form} from 'formik';
import FormField from '../../components/FormField';
import SelectField from '../../components/SelectField';
import LoadingDots from '../../components/LoadingDots';
import MaskedField from '../../components/MaskedField';
import Link from 'next/link';
import styles from '../../styles/pages/SignUp.module.css';
import useTranslation from 'next-translate/useTranslation';
import {signUpApiInterface} from '../../@types/signUpInterfaces';
import signUpValidator from '../../validators/signUpFormValidator';
import api from '../../services/api';
import {userEndpoint} from '../../services/endpoints';
import {removeSymbols} from '../../services/removeSymbols';
import {formatDate} from '../../services/formatDate';
import {toast} from 'react-toastify';

const SignUpForm: React.FC<any> = () => {
  const {t} = useTranslation();
  const signUpSchema = signUpValidator();
  const genderList = [
    {key: t('signup:genderChoice'), value: ''},
    {key: t('signup:male'), value: 'M'},
    {key: t('signup:female'), value: 'F'},
  ];
  let toastMessage = '';

  const body = {} as signUpApiInterface;

  const handleSubmit = async ({cellphone, cpf, ...values}) => {
    body.cpf = removeSymbols(cpf);
    body.dataCadastro = formatDate(new Date().toUTCString());
    body.dataNascimento = values.birthDate;
    body.email = values.email;
    body.genero = values.gender;
    body.nome = values.name;
    body.numeroCelular = removeSymbols(cellphone);
    body.senha = values.password;
    body.userName = values.username;
    toastMessage = t('signup:signUpSuccess');
    try {
      await api.post(userEndpoint, body);
      toast.success(toastMessage);
    } catch (err) {
      if (err.response.status == 409) {
        toastMessage = t('signup:signUpConflictError');
        toast.error(toastMessage);
      } else {
        toastMessage = t('signup:signUpError');
        toast.error(toastMessage);
      }
    }
  };

  return (
    <Formik
      initialValues={{
        username: '',
        email: '',
        password: '',
        name: '',
        gender: '',
        cpf: '',
        birthDate: '',
        cellphone: '',
      }}
      validateOnMount
      validationSchema={signUpSchema}
      onSubmit={handleSubmit}
    >
      {(formik) => (
        <div className={styles.content}>
          <h1>{t('common:signup')}</h1>
          <Form className={styles.form}>
            <h4>{t('signup:accessData')}</h4>
            <FormField
              name="username"
              label={t('signup:username')}
              placeholder={t('signup:usernamePlaceholder')}
            />
            <FormField
              className={styles.formField}
              name="email"
              label={t('signup:email')}
              placeholder={t('signup:emailPlaceholder')}
            />
            <FormField
              className={styles.formField}
              name="password"
              label={t('signup:password')}
              placeholder={t('signup:passwordPlaceholder')}
              type="password"
            />
            <h4>{t('signup:personalData')}</h4>
            <FormField
              className={styles.formField}
              name="name"
              label={t('signup:name')}
              placeholder={t('signup:namePlaceholder')}
            />
            <SelectField
              label={t('signup:gender')}
              name="gender"
              options={genderList}
            />
            <MaskedField
              className={styles.formField}
              name="cpf"
              label={'CPF'}
              placeholder={'xxx.xxx.xxx-xx'}
              format="###.###.###-##"
              mask="_"
            />
            <FormField
              className={styles.formField}
              name="birthDate"
              label={t('signup:birthDate')}
              type="date"
            />
            <MaskedField
              className={styles.formField}
              name="cellphone"
              label={t('signup:cellphone')}
              placeholder={'(xx) xxxxx-xxxx'}
              format="(##) #####-####"
              mask="_"
            />
            <button
              className={styles.submitBtn}
              type="button"
              disabled={!formik.isValid || formik.isSubmitting}
              onClick={(event) => {
                event.preventDefault();
                formik.handleSubmit();
              }}
            >
              {formik.isSubmitting ? <LoadingDots /> : t('signup:submit')}
            </button>
            <Link href="/login">
              <a>{t('signup:loginRedirect')}</a>
            </Link>
          </Form>
        </div>
      )}
    </Formik>
  );
};

export default SignUpForm;
