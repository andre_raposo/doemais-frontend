import React from 'react';
import Navbar from '../components/Navbar';
import {ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import '../styles/globals.css';

function MyApp({Component, pageProps}): JSX.Element {
  return (
    <>
      <Navbar />
      <Component {...pageProps} />
      <ToastContainer
        position="bottom-center"
        autoClose={5000}
        hideProgressBar
        closeOnClick
      />
    </>
  );
}

export default MyApp;
