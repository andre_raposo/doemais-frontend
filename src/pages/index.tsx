import Head from 'next/head';
import useTranslation from 'next-translate/useTranslation';
import styles from '../styles/pages/Home.module.css';

export default function Home() {
  const {t} = useTranslation();

  return (
    <div className={styles.container}>
      <Head>
        <title>Doe+ | {t('common:home')}</title>
      </Head>
      <main className={styles.content}>
        <h1>{t('common:greeting')}</h1>
      </main>
    </div>
  );
}
