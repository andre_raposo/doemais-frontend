export const nameValidation = new RegExp('^[a-zA-Z_ ]*$');
export const phoneValidation = new RegExp(
    '\\([1-9]{2}\\) [9]{1}[0-9]{4}\\-[0-9]{4}',
);
