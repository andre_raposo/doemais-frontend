import styles from '../styles/components/LoadingDots.module.css';

const LoadingDots = () => (
  <div className={styles.ldsEllipsis}>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
  </div>
);

export default LoadingDots;
