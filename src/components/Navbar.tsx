import React from 'react';
import Link from 'next/link';
import {useRouter} from 'next/router';
import style from '../styles/components/Navbar.module.css';

const Navbar: React.FC<any> = () => {
  const router = useRouter();
  return (
    <nav className={style.navbar}>
      <div className={style.logoContainer}>
        <Link href="/">
          <img className={style.logo} src="img/logo192x192.png" />
        </Link>
      </div>
      <div className={style.navbarList}>
        <ul>
          {router.locales.map((locale) => (
            <li key={locale}>
              <Link href={router.asPath} locale={locale}>
                {locale}
              </Link>
            </li>
          ))}
        </ul>
      </div>
    </nav>
  );
};

export default Navbar;
