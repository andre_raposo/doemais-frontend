import {ErrorMessage, useField} from 'formik';
import NumberFormat from 'react-number-format';
import styles from '../styles/components/FormField.module.css';

const MaskedField = ({label, ...props}: any) => {
  const [field, meta] = useField(props);

  return (
    <div className={styles.formField}>
      <label>{label}</label>
      <NumberFormat
        {...field}
        {...props}
        className={meta.touched && meta.error ? 'inputError' : null}
      />
      <ErrorMessage component="div" name={field.name} className="errorMsg" />
    </div>
  );
};

export default MaskedField;
