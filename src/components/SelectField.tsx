import {Field, ErrorMessage} from 'formik';
import styles from '../styles/components/SelectField.module.css';

const SelectField: React.FC<any> = ({label, ...props}: any) => {
  return (
    <div className={styles.formField}>
      <label htmlFor={props.name}>{label}</label>
      <Field as="select" name={props.name} {...props}>
        {props.options.map(
            (option: { key: string; value: string }, index: number) => (
              <option key={index} value={option.value}>
                {option.key}
              </option>
            ),
        )}
      </Field>
      <ErrorMessage component="div" name={props.name} className="errorMsg" />
    </div>
  );
};

export default SelectField;
