export interface signUpApiInterface {
  cpf: string;
  dataCadastro: string;
  dataNascimento: string;
  email: string;
  genero: string;
  senha: string;
  nome: string;
  numeroCelular: string;
  userName: string;
}
